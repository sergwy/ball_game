This is a simple pygame example.  
* All balls are controlled simultaneously.
* Use arrow keys to control ball speed and direction.
* ESC pauses the game
* Use pause menu to restart or quit the game